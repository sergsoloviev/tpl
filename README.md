# package tpl

```go
package main

import (
	"fmt"
	"myproject/views"
	"bitbucket.org/sergsoloviev/tpl"
)

func main() {
	templater := tpl.Tpl{
		Logging:   true,
		Dir:       "/dir/with/template/files",
		Mode:      1, // 0 = Debug (reload templates on each Render)
		Extension: ".html",
		Required:  []string{"base", "index", "maps"},
	}
	templater.LoadTemplates()
	views.Tpl = &templater
        .
        .
        .
```

```go
package views

import (
	"fmt"
	"bitbucket.org/sergsoloviev/tpl"
)

var (
	Tpl     *tpl.Tpl
)

func Index() {
        .
        .
        .
	Tpl.Render(w, "index", contextVars)
```
