package tpl

import (
	"fmt"
	"html/template"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"path/filepath"
	"strings"
)

type Tpl struct {
	Tpls      map[string]*template.Template
	Required  []string
	Mode      int
	Dir       string
	Extension string
	Logging   bool
}

func (o *Tpl) SetExtension(ext string) {
	o.Extension = ext
}

func (o *Tpl) SetMode(mode int) {
	o.Mode = mode
}

func (o *Tpl) SetDir(tplDir string) {
	o.Dir = tplDir
}

func (o *Tpl) Render(w http.ResponseWriter, name string, context ...interface{}) error {
	if o.Mode == 0 {
		o.LoadTemplates()
	}

	var ctx interface{}
	if len(context) > 0 {
		ctx = context[0]
	}

	w.Header().Set("Content-Type", "text/html; charset=utf-8")
	if name == "404" {
		w.WriteHeader(http.StatusNotFound)
	}
	tpl := o.Tpls[name]
	return tpl.ExecuteTemplate(w, "base", ctx)
}

func (o *Tpl) HTML(tpl string) http.HandlerFunc {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		o.Render(w, tpl)
	})
}

func (o *Tpl) LoadTemplates() {
	if o.Logging {
		log.Println("Reading templates...")
	}
	o.readFiles(o.Dir)
}

func (o *Tpl) readFiles(dir string) {
	if dir[len(dir)-1:] != "/" {
		dir += "/"
	}
	files, err := ioutil.ReadDir(dir)
	if err != nil {
		log.Fatal(err)
	}
	for _, f := range files {
		name := dir + f.Name()
		fi, err := os.Stat(name)
		if err != nil {
			log.Fatal(err)
			return
		}
		switch mode := fi.Mode(); {
		case mode.IsDir():
			o.readFiles(name)
		case mode.IsRegular():
			extension := filepath.Ext(name)
			if extension == ".html" {
				tpl := strings.Replace(name, o.Dir, "", 1)
				if tpl[:1] == "/" {
					tpl = tpl[1:]
				}
				tpl = strings.Replace(tpl, o.Extension, "", 1)
				tplDir := o.Dir
				if tplDir[len(tplDir)-1:] != "/" {
					tplDir += "/"
				}
				if o.Logging {
					log.Println(tpl)
				}
				tplsFiles := []string{fmt.Sprintf("%s%s%s", tplDir, tpl, o.Extension)}
				for _, v := range o.Required {
					tplsFiles = append(tplsFiles, fmt.Sprintf("%s%s%s", tplDir, v, o.Extension))
				}
				if o.Tpls == nil {
					o.Tpls = make(map[string]*template.Template)
				}
				o.Tpls[tpl] = template.Must(template.ParseFiles(tplsFiles...))
			}
		}
	}
}
